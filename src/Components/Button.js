import React, { Component } from "react";

class Button extends Component {
  render() {
    return (
      <button
        id={this.props.id}
        style={this.props.style}
        onClick={this.props.onClick}
      >
        {" "}
        {this.props.symbole}
      </button>
    );
  }
}

export default Button;
