import React, { Component } from "react";
import Button from "./Components/Button";
class App extends Component {
  constructor() {
    super();
    this.state = {
      count: 0
    };
    this.handleCounter = this.handleCounter.bind(this);
  }

  handleCounter = event => {
    const prevSate = this.state.count;
    console.log(event.target.id);
    return event.target.id === "increment"
      ? this.setState({ count: prevSate + 1 })
      : this.state.count === 0
      ? 0
      : this.setState({ count: prevSate - 1 });
  };

  render() {
    return (
      <div>
        <h1> Count: {this.state.count} </h1>
        <Button
          id="increment"
          style={{ padding: "10px 30px" }}
          onClick={this.handleCounter}
          symbole="+"
        />
        <Button
          id="decrement"
          style={{ padding: "10px 30px" }}
          onClick={this.handleCounter}
          symbole="-"
        />
      </div>
    );
  }
}

export default App;
